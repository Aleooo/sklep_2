from django.conf import settings
from django.conf.urls.i18n import i18n_patterns
from django.conf.urls.static import static
from django.contrib import admin
from django.contrib.auth import views as auth_views
from django.urls import path, include


urlpatterns = i18n_patterns(
    # admin
    path('admin/', admin.site.urls),
    # apps
    path('', include('apps.shop.urls', namespace='shop')),
    path('analysis/', include('apps.analysis.urls', namespace='analysis')),
    path('cart/', include('apps.cart.urls', namespace='cart')),
    path('coupon/', include('apps.coupon.urls', namespace='coupon')),
    path('order/', include('apps.order.urls', namespace='order')),
    # authorization
    path('accounts/', include('allauth.urls')),
    path('social-auth/', include('social_django.urls', namespace='social')),
) + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
urlpatterns += static(settings.STATIC_URL) 
