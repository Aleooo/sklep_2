��    w      �  �   �      
     
     "
  	   +
     5
     =
     I
     R
     j
     o
     w
     �
     �
     �
     �
     �
     �
     �
     �
     �
     �
     �
     �
     �
     �
               5     ;     X  
   `     k  
   {  K   �  �   �     X     q     w     ~  	   �  �   �               0     B     G     L     \     i     v     �     �     �     �     �     �     �     �               "     /  "   J     m     ~     �     �     �     �     �     �     �  
   �     �     �     �     �     �                    "  ,   (  /   U  .   �  &   �  $   �  &      2   '     Z     _  @   h  A   �     �       H     2   Z     �     �     �     �  
   �     �     �     �     �     �     �     �     �     �                         !     (     +     2  �  ;            	   "     ,     2     C  
   K     V  	   ]     g     t     }     �     �  	   �     �     �     �     �     �     �     �     �     �               #     )     D     K     Q     f  F   v  �   �     I     d     q     ~     �  �   �          +     G     a  	   f     p     ~     �     �     �  	   �     �     �     �     �               2     >     L  "   [  (   ~     �  	   �     �     �     �     �     �     �          
       
   )     4     ;     L     R     ^     k     w  (   |  )   �  ,   �  %   �     "     <  -   V     �     �  *   �  I   �          %  ?   1  1   q     �     �     �     �     �     �     �     �  	          	                   1     >     L     \  	   `     j     q     t     �             +       A           ?   s   /      w       3   a       D   G           n      R   >   B   0   .   o   f                  \          N          =          P      k       u   )   I              ;   r               e         $       b   Y      U   [   #                   g          `              '   t      W       *   &   ^   p   2   i       L       :         S       ]   7   Z   	               -      <   1   q      h   X   m           F   ,                E   J   v      V          c   K   M         
   4       T       d   Q          H              C   (   "   6      j   9   O   l   !   _   8   @   5   %    20 to 50 50 to 75 75 to 150 ACCOUNT ADD TO CART ANALYSIS Already have an account BACK BUY NOW Bill nr. Bills Buyer CANCEL CHANGE CONTINUE Cart Change Click below link: Confirm Password Country Courier Delivery offer Description Details of the recipient Discount Don't have an account Email Enter the new password twice Filters First Name Forgot password GO TO CART I declare that I know and accept the provisions of the Regulations Sklep_2. If you haven't the email message make sure on the form of password reset enter address e-mail provided when creating the user account Incorrect Mobile Number! LOGIN LOGOUT Language Last Name Link to reset password is incorrect because it has probably been used before. You must again to start the procedure password reset. Log in Login with Facebook Login with Google MAIN Next Number of items Only letters Only numbers PAY AND POST PAYMENT Parcel locker Password Password change Password change done Password has been changed  Password reset Passwords must be the same Payment Personal data Phone Number Please enter a valid email Please enter a valid street number Popular for sale Previous Prices Product reviews Products Register Reset Password Reset password SEARCH SEND EMAIL SIGN UP SUBMIT Search Sign up Street Street Number Summary TO PAY TOTAL The country must start with a capital letter The first name must start with a capital letter The last name must start with a capital letter The password has been defined. You can The two password fields didn`t match The two password fields didn’t match The username or password is incorrect. Try again.  Town Username We received the a password reset request for users use the email We send you email with instructions let you create a new password You have added a product Your address Your password can’t be too similar to your other personal information. Your password must contain at least 8 characters.  Your username: ZIP Code ZIP code add cart value change coupon email english filter for each from greater then 150 in now less than 20 login or paid polish to unpaid username Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=4; plural=(n==1 ? 0 : (n%10>=2 && n%10<=4) && (n%100<12 || n%100>14) ? 1 : n!=1 && (n%10>=0 && n%10<=1) || (n%10>=5 && n%10<=9) || (n%100>=12 && n%100<=14) ? 2 : 3);
 20 do 50 50 do 75 75 do 150 KONTO DODAJ DO KOSZYKA ANALIZA Masz konto WRÓĆ KUP TERAZ Rachunek nr. Rachunki Dane odbiorcy przesyłki ANULUJ ZMIEŃ KONTYNUUJ Koszyk Zmień Kliknij poniższy link Potwierdź Hasło Kraj Kurier Dostawa Opis Dane odbiorcy Zniżka Nie masz konta Email Wpisz dwa razy nowe hasło Filtry Imię Zapomniałeś hasła IDŹ DO KOSZYKA Oświadczam, że znam i akceptuję postanowienia w regulaminie Sklep_2 Jeśli nie otrzymałeś wiadomości email, upewnij się, że wpisałeś w formularzu email, który był dostarczony podczas tworzenia konta Nieporawny Number Telefonu Zaloguj się Wyloguj się Język Nazwisko Link do resetowania hasła jest niepoprawny ponieważ prawdopodobnie został już użyty. Musisz ponownie zacząć procedure resetowania hasła Zaloguj się Zaloguj się przez Facebook Zaloguj się przez Google MENU Następny Liczba rzeczy Tylko litery Tylko liczby ZAPŁAĆ I WYŚLIJ PŁATNOŚĆ Paczkomat Hasło Zmiana Hasła Zmiana hasła wykonana Hasło zostało zmienione Reset Hasła Hasła muszą być takie same Płatność Dane Osobiste Numer Telefonu Proszę wprowadzić poprawny email Proszę wprowadzić poprawny numer ulicy Popularne w sprzedaży Poprzedni Ceny Opinie Produkty Rejestracja Reset Hasła Reset hasła SZUKAJ WYŚLIJ EMAIL ZAREJESTRUJ SIĘ POTWIERDŹ Szukaj Zarejestruj się Ulica Numer Ulicy PODSUMOWANIE DO ZAPŁATY SUMA Kraj musi się zaczynać wielką literą Imię musi się zaczynać wielką literą Nazwisko musi się zaczynać wielką literą Hasło zostało zdefiniowane. Możesz Hasła nie są identyczne Hasła nie są identyczne Nazwa użytkownika lub hasło są niepoprawne Miasto Nazwa Użytkownika Otrzymaliśmy żądanie resetowania hasła Wysłaliśmy Ci email z instrukcjami pozwalającymi stworzyć nowe hasło Dodałeś produkt Twój adres Twoje hasło nie może być podobne do twoich danych osobistych Twoje hasłó musi zawierać conajmniej 8 znaków Nazwa twojego użytkownika KOD pocztowy KOD pocztowy dodaj Wartość koszyka zmień kupon email angielski filtr za każdy od więcej niż 150 w tej chwili mniej niż 20 zalogować się lub opłacone polski do nieopłacone nazwa użytkownika 